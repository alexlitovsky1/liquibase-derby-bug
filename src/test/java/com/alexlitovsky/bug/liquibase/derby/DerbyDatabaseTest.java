package com.alexlitovsky.bug.liquibase.derby;

import org.apache.derby.jdbc.EmbeddedDriver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import liquibase.database.core.DerbyDatabase;

public class DerbyDatabaseTest {
	
	private static final String JDBC_URL = "jdbc:derby:memory:test";
	
	@Test
	public void testGetDefaultDriver() {
		DerbyDatabase database = new DerbyDatabase();
		Assertions.assertEquals(EmbeddedDriver.class.getName(), database.getDefaultDriver(JDBC_URL));
	}
}
